/***************************************************************************//**
 * @file
 * @brief Core application logic.
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include "em_common.h"
#include "sl_app_assert.h"
#include "sl_bluetooth.h"
#include "gatt_db.h"
#include "app.h"

//#include <em_device.h>

//#include "sl_bluetooth.h"
//#include "sl_bt_stack_config.h"
//#include "sl_bt_rtos_adaptation.h"
#include "FreeRTOS.h"
#include "event_groups.h"
#include "sl_emlib_gpio_init_Pin1_config.h"
#include "sl_emlib_gpio_init_Pin2_config.h"
#include "em_gpio.h"




EventGroupHandle_t led_event_flags;
#define LED_ON_EVT (0x01)
#define LED_OFF_EVT (0x02)

// The advertising set handle allocated from Bluetooth stack.
static uint8_t advertising_set_handle = 0xff;

volatile int testbka = 0;

static void LedBlinkTask(void *p_arg)
{
    (void)p_arg;

    while (1)
    {
        EventBits_t uxBits;

        uxBits = xEventGroupWaitBits(led_event_flags,   /* The event group being tested. */
                                     LED_ON_EVT|LED_OFF_EVT, /* The bits within the event group to wait for. */
                                     pdTRUE,         /* LED_FLIP_EVT should be cleared before returning. */
                                     pdFALSE,         /* Wait for all the bits to be set, not needed for single bit. */
                                     portMAX_DELAY); /* Wait for maximum duration for bit to be set. With 1 ms tick,
                                                        portMAX_DELAY will result in wait of 50 days*/

        if (uxBits & LED_OFF_EVT)
        {
            GPIO_PinOutSet(SL_EMLIB_GPIO_INIT_PIN1_PORT, SL_EMLIB_GPIO_INIT_PIN1_PIN);
        }
        else if  (uxBits & LED_ON_EVT)
        {
            testbka++;
            GPIO_PinOutClear(SL_EMLIB_GPIO_INIT_PIN1_PORT, SL_EMLIB_GPIO_INIT_PIN1_PIN);
        }
    }
}

#define LED_STACK_SIZE 1000
static TaskHandle_t LedTaskHandle = NULL;

SL_WEAK void app_init(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application init code here!                         //
  // This is called once during start-up.                                    //
  /////////////////////////////////////////////////////////////////////////////
#if 1
  led_event_flags = xEventGroupCreate();


  // create tasks for Linklayer
  xTaskCreate(LedBlinkTask,                              /* Function that implements the task. */
              "LedBlinkTask",                           /* Text name for the task. */
              LED_STACK_SIZE / sizeof(StackType_t), /* Number of indexes in the xStack array. */
              NULL,                                       /* Parameter passed into the task. */
              1,                     /* Priority at which the task is created. */
              &LedTaskHandle);
#endif
}

/**************************************************************************//**
 * Application Process Action.
 *****************************************************************************/
SL_WEAK void app_process_action(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application code here!                              //
  // This is called infinitely.                                              //
  // Do not call blocking functions from here!                               //
  /////////////////////////////////////////////////////////////////////////////
}

/**************************************************************************//**
 * Bluetooth stack event handler.
 * This overrides the dummy weak implementation.
 *
 * @param[in] evt Event coming from the Bluetooth stack.
 *****************************************************************************/
void sl_bt_on_event(sl_bt_msg_t *evt)
{
  sl_status_t sc;
  bd_addr address;
  uint8_t address_type;
  uint8_t system_id[8];
  int16_t set_power;


  switch (SL_BT_MSG_ID(evt->header)) {
    // -------------------------------
    // This event indicates the device has started and the radio is ready.
    // Do not call any stack command before receiving this boot event!
    case sl_bt_evt_system_boot_id:


      // Extract unique ID from BT Address.
      sc = sl_bt_system_get_identity_address(&address, &address_type);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to get Bluetooth address\n",
                    (int)sc);

      // Pad and reverse unique ID to get System ID.
      system_id[0] = address.addr[5];
      system_id[1] = address.addr[4];
      system_id[2] = address.addr[3];
      system_id[3] = 0xFF;
      system_id[4] = 0xFE;
      system_id[5] = address.addr[2];
      system_id[6] = address.addr[1];
      system_id[7] = address.addr[0];

      sc = sl_bt_gatt_server_write_attribute_value(gattdb_system_id,
                                                   0,
                                                   sizeof(system_id),
                                                   system_id);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to write attribute\n",
                    (int)sc);

      // Create an advertising set.
      sc = sl_bt_advertiser_create_set(&advertising_set_handle);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to create advertising set\n",
                    (int)sc);

      // Set advertising interval to 100ms.
      sc = sl_bt_advertiser_set_timing(
        advertising_set_handle,
        4800, // min. adv. interval (milliseconds * 1.6)
        4800, // max. adv. interval (milliseconds * 1.6)
        0,   // adv. duration
        0);  // max. num. adv. events
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to set advertising timing\n",
                    (int)sc);
      // Start general advertising and enable connections.
      sl_bt_advertiser_set_tx_power(advertising_set_handle, 0, &set_power);



      sc = sl_bt_advertiser_start(
        advertising_set_handle,
        advertiser_general_discoverable,
        advertiser_connectable_scannable);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start advertising\n",
                    (int)sc);
      break;

    // -------------------------------
    // This event indicates that a new connection was opened.
    case sl_bt_evt_connection_opened_id:
      xEventGroupSetBits(led_event_flags, LED_ON_EVT);
      break;

    // -------------------------------
    // This event indicates that a connection was closed.
    case sl_bt_evt_connection_closed_id:

      // Restart advertising after client has disconnected.
      sc = sl_bt_advertiser_start(
        advertising_set_handle,
        advertiser_general_discoverable,
        advertiser_connectable_scannable);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start advertising\n",
                    (int)sc);
      xEventGroupSetBits(led_event_flags, LED_OFF_EVT);
      break;

    ///////////////////////////////////////////////////////////////////////////
    // Add additional event handlers here as your application requires!      //
    ///////////////////////////////////////////////////////////////////////////

    // -------------------------------
    // Default event handler.
    default:
      break;
  }
}
