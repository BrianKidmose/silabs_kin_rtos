#include "sl_emlib_gpio_simple_init.h"
#include "sl_emlib_gpio_init_Pin1_config.h"
#include "sl_emlib_gpio_init_Pin2_config.h"
#include "em_gpio.h"
#include "em_cmu.h"

void sl_emlib_gpio_simple_init(void)
{
  CMU_ClockEnable(cmuClock_GPIO, true);
  GPIO_PinModeSet(SL_EMLIB_GPIO_INIT_PIN1_PORT,
                  SL_EMLIB_GPIO_INIT_PIN1_PIN,
                  SL_EMLIB_GPIO_INIT_PIN1_MODE,
                  SL_EMLIB_GPIO_INIT_PIN1_DOUT);

  GPIO_PinModeSet(SL_EMLIB_GPIO_INIT_PIN2_PORT,
                  SL_EMLIB_GPIO_INIT_PIN2_PIN,
                  SL_EMLIB_GPIO_INIT_PIN2_MODE,
                  SL_EMLIB_GPIO_INIT_PIN2_DOUT);
}
